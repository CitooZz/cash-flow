from django.apps import AppConfig

class CustomAccount(AppConfig):
    name = 'cash.apps.account'
    label = 'custom_account'