from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as default_logout
from django.utils.translation import ugettext_lazy as _
from cash.apps.account.forms import SignupForm

import account.views
import account.forms


class LoginView(account.views.LoginView):
    form_class = account.forms.LoginEmailForm


class SignupView(account.views.SignupView):
    form_class = SignupForm


@login_required
def logout(request):
    default_logout(request)
    return redirect(reverse('account_login'))
