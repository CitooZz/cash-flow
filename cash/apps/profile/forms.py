from django import forms
from cash.apps.profile.models import Profile, Note
from cash.apps.transaction.models import CategoryTransaction, BookTransaction


class ProfileForm(forms.ModelForm):
    username = forms.CharField(max_length=20)
    email = forms.EmailField()

    class Meta:
        model = Profile
        exclude = ('user', )


class CategoryForm(forms.ModelForm):

    class Meta:
        model = CategoryTransaction
        fields = ('name', )


class BookTransactionForm(forms.ModelForm):

    class Meta:
        model = BookTransaction
        exclude = ('user', )


class NoteForm(forms.ModelForm):

    class Meta:
        model = Note
        exclude = ('creator', )
