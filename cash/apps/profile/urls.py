from django.conf.urls import patterns, url


urlpatterns = patterns("cash.apps.profile.views",
	url(r"^profile/$", "edit_profile", name="edit_profile"),
	url(r"^category/$", "profile_category", name="profile_category"),
	url(r"^category/create/expense/$", "create_category", {'type': 'expense'}, name='create_expense_category'),
	url(r"^category/(\d+)/edit/expense/$", "create_category", {'type': 'expense'}, name='edit_expense_category'),
	url(r"^category/create/revenue/$", "create_category", {'type': 'revenue'}, name='create_revenue_category'),
	url(r"^category/(\d+)/edit/revenue/$", "create_category", {'type': 'revenue'}, name='edit_revenue_category'),
	url(r"^category/(\d+)/remove/$", "remove_category", name="remove_category"),
	url(r"^book/$", "book_setting", name="book_setting"),
	url(r"^note/$", "note", name="note"),
	url(r"^note/(\d+)/remove/$", "remove_note", name="remove_note"),
)