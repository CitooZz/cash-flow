from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.decorators.http import require_POST
from django.contrib import messages
from django.core.urlresolvers import reverse

from cash.apps.profile.forms import ProfileForm, CategoryForm, BookTransactionForm, NoteForm
from cash.apps.profile.models import Note
from cash.apps.transaction.models import CategoryTransaction, CategoryType, BookTransaction


@login_required
def edit_profile(request):
    user = request.user
    form = ProfileForm(request.POST or None, instance=user.profile,
                       initial={'username': user.username, 'email': user.email})

    if request.POST:
        if form.is_valid():
            user.username = form.cleaned_data.get('username')
            user.email = form.cleaned_data.get('email')
            user.save()

            form.save()
            messages.success(request, "Your profile has been saved.")

    context = {
        'form': form
    }
    return render(request, "profile/edit_profile.html", context)


@login_required
def profile_category(request):
    context = {
        'form': CategoryForm(),
        'expense_categories': CategoryTransaction.objects.filter_by_creator_and_type(request.user, CategoryType.EXPENSE),
        'revenue_categories': CategoryTransaction.objects.filter_by_creator_and_type(request.user, CategoryType.REVENUE)
    }
    return render(request, "profile/profile_category.html", context)


@require_POST
@login_required
def create_category(request, category_id=None, type='expense'):
    category = get_object_or_404(
        CategoryTransaction, id=category_id) if category_id else None
    form = CategoryForm(request.POST, instance=category)

    if form.is_valid():
        category = form.save(commit=False)
        if not category_id:
            category.creator = request.user

            if type == 'revenue':
                category.type = CategoryType.REVENUE

        category.save()
        messages.success(request, 'Category has been saved.')

    return redirect(reverse('profile_category'))


@require_POST
@login_required
def remove_category(request, category_id):
    category = get_object_or_404(CategoryTransaction, id=category_id)
    category.delete()

    messages.success(request, "Category has been deleted.")
    return redirect(reverse('profile_category'))


@login_required
def book_setting(request):
    book = None
    qs = BookTransaction.objects.filter(user=request.user)
    if qs.exists():
        book = qs[0]

    if request.POST:
        form = BookTransactionForm(request.POST, instance=book)
        if form.is_valid():
            book = form.save(commit=False)
            book.user = request.user
            book.save()
            messages.success(request, "Book transaction has been saved.")

    else:
        form = BookTransactionForm(instance=book)

    context = {
        'form': form
    }
    return render(request, "profile/book_transaction.html", context)


@login_required
def note(request):
    form = NoteForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            note = form.save(commit=False)
            note.creator = request.user
            note.save()

            messages.success(request, "Note has been saved.")
        print form.errors
    context = {
        'form': form,
        'notes': Note.objects.filter(creator=request.user)
    }
    return render(request, 'profile/note.html', context)


@require_POST
@login_required
def remove_note(request, note_id):
    note = get_object_or_404(Note, id=note_id)
    note.delete()

    messages.success(request, "Note has been deleted.")
    return redirect(reverse('note'))
