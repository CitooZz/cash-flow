from django.conf.urls import patterns, url

urlpatterns = patterns('cash.apps.report.views',
	url(r"^$", 'report', name='report'),
)