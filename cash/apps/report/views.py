from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def report(request):
	context = {}
	return render(request, 'report/report.html', context)