from django import forms

from cash.apps.transaction.models import Transaction


class TransactionForm(forms.ModelForm):

    class Meta:
        model = Transaction
        exclude = ('book', 'total_balance', )
