from django.db import models
from django.contrib.auth.models import User


class BookTransaction(models.Model):
    user = models.ForeignKey(User, related_name="book_transaction")
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=150, null=True, blank=True)
    initial_balance = models.FloatField(default=0)

    def __unicode__(self):
        return self.name


class CustomQuerySet(models.Manager):

    def filter_by_creator_and_type(self, user, type):
        return self.filter(creator=user, type=type)


class CategoryType:
    EXPENSE = 1
    REVENUE = 2

CATEGORY_TYPE_CHOICES = (
    (CategoryType.EXPENSE, "Expense"),
    (CategoryType.REVENUE, "Revenue")
)


class CategoryTransaction(models.Model):
    creator = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    type = models.IntegerField(choices=CATEGORY_TYPE_CHOICES, default=1)
    objects = CustomQuerySet()

    def __unicode__(self):
        return self.name


class Transaction(models.Model):
    book = models.ForeignKey(BookTransaction, related_name="transactions")
    timestamp = models.DateTimeField()
    category = models.ForeignKey(CategoryTransaction)
    description = models.CharField(max_length=100, null=True, blank=True)
    total_transaction = models.FloatField()
    total_balance = models.FloatField(default=0)
