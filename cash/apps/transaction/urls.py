from django.conf.urls import patterns, url


urlpatterns = patterns( 'cash.apps.transaction.views',
	url(r'^$', 'transaction', name='transaction'),
	url(r"^create/$", "create_transaction", name='create_transaction'),
	url(r"^edit/(\d+)/$", "create_transaction", name='edit_transaction'),
)
