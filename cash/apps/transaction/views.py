from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.contrib import messages
from django.core.urlresolvers import reverse

from cash.apps.transaction.forms import TransactionForm
from cash.apps.transaction.models import Transaction, CategoryType, CategoryTransaction, BookTransaction


@login_required
def transaction(request):
    form_expense = TransactionForm()
    form_revenue = TransactionForm()

    form_expense.fields['category'].queryset = CategoryTransaction.objects.filter_by_creator_and_type(
        request.user, CategoryType.EXPENSE)

    form_revenue.fields['category'].queryset = CategoryTransaction.objects.filter_by_creator_and_type(
        request.user, CategoryType.REVENUE)

    book = BookTransaction.objects.filter(user=request.user)
    context = {
        'form_expense': form_expense,
        'form_revenue': form_revenue,
        'transactions': Transaction.objects.filter(book=book)
    }
    return render(request, 'transaction/transaction.html', context)


@require_POST
@login_required
def create_transaction(request, transaction_id=None):
    book = get_object_or_404(
        BookTransaction, user=request.user)
    transaction = get_object_or_404(
        Transaction, id=transaction_id) if transaction_id else None
    form = TransactionForm(request.POST, instance=transaction)

    if form.is_valid():
        transaction = form.save(commit=False)
        if not transaction_id:
            transaction.book = book

        if transaction.category == CategoryType.REVENUE:
            book.initial_balance += transaction.total_transaction
        else:
            book.initial_balance -= transaction.total_transaction
        book.save()
        transaction.total_balance = book.initial_balance
        transaction.save()

        messages.success(request, "Transaction has been saved.")

    print form.errors

    return redirect(reverse('transaction'))
