"""
Django settings for cash project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'y8#ys@3-x5(-r-b4pl@zdy_9uetzi-2z&(3qy7y@8q=bt7@zr1'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

SITE_ID = 1

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'havizvaisal@gmail.com'
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 587

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # external apps
    'account',

    # internal apps
    'cash.apps.account',
    'cash.apps.profile',
    'cash.apps.transaction',
    'cash.apps.report',


)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
   #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
]

ROOT_URLCONF = 'cash.urls'

WSGI_APPLICATION = 'cash.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "static")
STATIC_URL = "/site_media/static/"

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = [
    os.path.join(PACKAGE_ROOT, "templates"),
]

MEDIA_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "media")
MEDIA_URL = "/site_media/media/"

STATIC_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "static")
STATIC_URL = "/site_media/static/"
STATICFILES_DIRS = [
    os.path.join(PACKAGE_ROOT, "static"),
]
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


ACCOUNT_OPEN_SIGNUP = True
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = False
ACCOUNT_EMAIL_CONFIRMATION_EMAIL = True
ACCOUNT_LOGIN_REDIRECT_URL = "book_setting"
ACCOUNT_SIGNUP_REDIRECT_URL = "account_login"
ACCOUNT_LOGOUT_REDIRECT_URL = "account_login"
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 2

LOGIN_URL = "/account/login/"
LOGIN_ERROR_URL = "/account/login"
LOGIN_REDIRECT_URL = "/"
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = "/profile_edit/?from=social"

# django social auth setting
# to obtain these visit http://developers.facebook.com/setup/
FACEBOOK_APP_ID = "371572226323266"
FACEBOOK_API_SECRET = "fa18f989f59568b9d5418579883c2cfd"
TWITTER_CONSUMER_KEY = "DTZQkzXDwJUaGTeWlldnAmXEW"
TWITTER_CONSUMER_SECRET = "uNKxqHbOUjGTUTVQzw4dUVgmteANGaqCtg7g1btgCFyYIWdNoa"

# to obtain these visit https://code.google.com/apis/console/?pli=1#project:798920728513:access
GOOGLE_OAUTH2_CLIENT_ID = "974948840064-oe14086psfsjapnrva608l44r569vcl8.apps.googleusercontent.com"
GOOGLE_OAUTH2_CLIENT_SECRET = "xT5OQCCtmwYXh-0OE1ujmlSW"


# Add facebook email
FACEBOOK_EXTENDED_PERMISSIONS = ["email", "user_about_me", "user_birthday", "user_website"]


GOOGLE_OAUTH2_EXTRA_SCOPE = [
    "https://www.googleapis.com/auth/userinfo.profile",
    "https://www.googleapis.com/auth/userinfo.email"]

GOOGLE_OAUTH2_EXTRA_FIELD_SELECTORS = ["family_name", "given_name", "email", ]
GOOGLE_OAUTH2_EXTRA_DATA = [("family_name", "last_name"), ("given_name", "first_name"), ("email", "email")]

FACEBOOK_EXTRA_DATA = [("first_name", "first_name"), ("last_name", "last_name"), ("email", "email"),]

AUTHENTICATION_BACKENDS = [
    "social_auth.backends.facebook.FacebookBackend",
    "social_auth.backends.google.GoogleOAuth2Backend",
    "social_auth.backends.twitter.TwitterBackend",
    "django.contrib.auth.backends.ModelBackend",
    #"cash.apps.account.backend.EmailAUthenticationBackend",
    "account.auth_backends.EmailAuthenticationBackend",
]

# Session settings
SESSION_COOKIE_NAME = "sessionid"
SESSION_ENGINE = "django.contrib.sessions.backends.db"
SESSION_COOKIE_AGE = 60 * 60 * 24 * 7 * 2   #two weeks
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_COOKIE_SECURE = False
SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'
