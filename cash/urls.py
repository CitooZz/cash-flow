from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import RedirectView

from cash.apps.account.views import LoginView, SignupView, logout

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cash.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include("cash.apps.account.urls")),
    url(r'^setting/', include("cash.apps.profile.urls")),

    url(r"^$", RedirectView.as_view(pattern_name="account_login", permanent=True)),

    url(r"^account/login/$", LoginView.as_view(), name="account_login"),
	url(r"^account/signup/$", SignupView.as_view(), name="account_signup"),
    url(r"^account/logout/$", logout, name="account_logout"),
	url(r"^account/", include("account.urls")),

    url(r"^transaction/", include("cash.apps.transaction.urls")),
    url(r"^report/$", include("cash.apps.report.urls")),
)
